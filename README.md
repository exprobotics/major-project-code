# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
This repository contains the code written and implemented by Group 4 for the MTRX5700 Major Project, by the members:
###

* Tejasva Saboo
* Quinn Robinson
* Julian Heath
* Venkatesh Chenniappan

### NOTE: ###
The file 'ik_service_client_CV_movearm1_Commented.py' demonstrates an example of how to use the MoveIt package
for inverse kinematics of the robot arms. It was used as a test file.
