#! /usr/bin/python

# rospy for the subscriber
import rospy
import time
# ROS Image message
from sensor_msgs.msg import Image
# IR Sensor message
from sensor_msgs.msg import Range
# ROS Image message -> OpenCV2 image converter
from cv_bridge import CvBridge, CvBridgeError
# OpenCV2 for saving an image
import cv2
import numpy as np
# Baxter interface class
import baxter_interface
import baxter_external_devices
from baxter_interface import CHECK_VERSION

# Instantiate CvBridge
bridge = CvBridge()

#GLOBAL VARIABLES
# Variable for specifying left arm position
global home_left
#left arm gripper control
global grip_left
#Flag to control image processing
global clik
# Infra-red sensor reading
global IRvalue


def ir_callback(msg):
	global IRvalue
	IRvalue=msg.range


def image_callback(msg):
    global home_left
    global grip_left
    global clik
	# Low and high valiues for color thresholding
    global colorLow, colorHigh
    global IRvalue
    #print("Received an image!")
    try:
        # Convert your ROS Image message to OpenCV2
	
        	cv2_img = bridge.imgmsg_to_cv2(msg, "bgr8")

    except CvBridgeError, e:
        print(e)
    else:
    # Convert to HSV Image
    	toHSV=cv2.cvtColor(cv2_img, cv2.COLOR_BGR2HSV)


		mask=cv2.inRange(toHSV,colorLow,colorHigh)
		imagearray=np.array(mask)
		result = np.count_nonzero(imagearray)
		print(result)

		#print(mask.shape)
		M=cv2.moments(mask)
	

	#Calculation of centroid
		if (result <10000):
			print("the chosen color is not in the camera view range. Moving left")
			#Incremental step
			deltaX=0.02
			print('left_s0',home_left['left_s0'])
				#home_left = {'left_s0': home_left['left_s0']+deltaX, 'left_s1': -1.00, 'left_w0': 0.67,
       				#'left_w1': 1.03, 'left_w2': -0.50, 'left_e0': -1.18, 'left_e1':
       				# 1.94}
			home_left = {'left_s0': home_left['left_s0']+deltaX, 'left_s1': -0.0000, 'left_w0':
					-1.5, 'left_w1': 1.5, 'left_w2': 0.0, 'left_e0': 1.57,
					'left_e1': 0.0}
			print('updatedleft_s0',home_left['left_s0'])
			limb_left.move_to_joint_positions(home_left)
		else:
			cx=int(M["m10"]/M["m00"])
			cy=int(M["m01"]/M["m00"])
	#circled=cv2.circle(mask,(cx,cy),10,(0,0,255),5)
	#cv2.imshow('circled',circled)
			print('cx',cx)
			print('cy',cy)
			r=range(140,500)
		if clik==0:
			if cx in r:
				print ('The camera is viewing the centre of the block. You can actuate the gripper')
				#closes the gripper
				grip_left.calibrate()
				
				home_left = {'left_s0': home_left['left_s0'], 'left_s1': 0.1000, 'left_w0': 
					-1.5, 'left_w1': 1.5, 'left_w2': 0.0, 'left_e0': 1.57,
					'left_e1': 0.0}
				limb_left.move_to_joint_positions(home_left)
				grip_left.close()
				home_left = {'left_s0': -0.90, 'left_s1': 0.05000, 'left_w0': 
					-1.5, 'left_w1': 0, 'left_w2': 0.0, 'left_e0': 1.57,
					'left_e1': 0.0}
				limb_left.move_to_joint_positions(home_left)
				#grip_left.close()
				clik=1
				time.sleep(5)
				if (IRvalue <0.30):

					print('picked up the object.The IR value is',IRvalue)
				else:
					print('Sorry, I dropped the box. The IR reading is',IRvalue)
				time.sleep(5)
				grip_left.open()
				#clik=0
				
		#else:
		#	Code to move the arm to the left if the centroid is less than 140
			elif cx < 140:
				print('Need to centre the block.Moving the left arm towards left')
				deltaX=0.01
				print('left_s0',home_left['left_s0'])
								home_left = {'left_s0': home_left['left_s0']+deltaX, 'left_s1': -0.0000, 'left_w0':
					-1.5, 'left_w1': 1.5, 'left_w2': 0.0, 'left_e0': 1.57,
					'left_e1': 0.0}
				print('updatedleft_s0',home_left['left_s0'])
				limb_left.move_to_joint_positions(home_left)

		#	Code to move the arm to the right if the centroid is more than 500
			elif cx >500:
				print('Need to centre the block. Moving the left arm towards right')
				deltaX=-0.01
				print('left_s0',home_left['left_s0'])
				#home_left = {'left_s0': home_left['left_s0']+deltaX, 'left_s1': -1.00, 'left_w0': 0.67,
       				#'left_w1': 1.03, 'left_w2': -0.50, 'left_e0': -1.18, 'left_e1':
       				# 1.94}
				home_left = {'left_s0': home_left['left_s0']+deltaX, 'left_s1': 0.0000, 'left_w0': 
					-1.5, 'left_w1': 1.5, 'left_w2': 0.0, 'left_e0': 1.57,
					'left_e1': 0.0}
				print('updatedleft_s0',home_left['left_s0'])
				limb_left.move_to_joint_positions(home_left)

	#Display the mask
	cv2.imshow('mask',mask)
	#Display the original image and the masked image (color thresholded)
	selected=cv2.bitwise_and(cv2_img,cv2_img, mask=mask)
	vis=np.concatenate((cv2_img,selected),axis=1)
	cv2.imwrite('OriginalandProcessed.png',vis)
	

	cv2.waitKey(0)
	cv2.destroyAllWindows()
	#return()

def image_listen():
    global home_left
    global grip_left
    global clik
    clik=0
    # Define your image topic
    #image_topic = "/cameras/head_camera/image"
    #image_topic = "/cameras/right_hand_camera/image"
    image_topic = "/cameras/left_hand_camera/image"
    ir_topic="/robot/range/left_hand_range/state"
    # Set up your subscriber and define its callback
    rospy.Subscriber(image_topic, Image, image_callback)
    rospy.Subscriber(ir_topic, Range, ir_callback)


if __name__ == '__main__':
    global home_left
    global grip_left
    global clik
    global colorLow, colorHigh
    clik=0
    rospy.init_node('image_listener')
    limb_right = baxter_interface.Limb('right')
    limb_left = baxter_interface.Limb('left')
    grip_left = baxter_interface.Gripper('left', CHECK_VERSION)
    grip_right = baxter_interface.Gripper('right', CHECK_VERSION)
    grip_left.calibrate()
    #Move the arms to the home position (left arm for object picking, right for imaging the face of the user)
    home_right = {'right_s0': 0.70, 'right_s1': 0.00, 'right_w0': -
	0.0, 'right_w1': 0.0, 'right_w2': 0.0, 'right_e0': 1.57,
	'right_e1': 0.0}

    home_left = {'left_s0': -0.90, 'left_s1': -0.00000, 'left_w0': 
	-1.5, 'left_w1': 1.5, 'left_w2': 0.0, 'left_e0': 1.57,
	'left_e1': 0.0}
    limb_right.move_to_joint_positions(home_right)
    limb_left.move_to_joint_positions(home_left)

    #EMOTION DETECTION code will replace the following block.
    a=input('please enter a value between 1 and 3\n')
	#print(toHSV.shape)
    if a==1:
	#YELLOW - Emotion- HAPPY
	colorLow=np.array([20,100,100])
	colorHigh=np.array([30,255,255])
    elif a==2:
	#RED - Emotion - ANGRY
	colorLow=np.array([0,70,50])
	colorHigh=np.array([10,255,255])
    elif a==3:
	#BLUE - Emotion - SAD
		#colorLow=np.array([110,50,50])
	colorLow=np.array([50,120,150])
	colorHigh=np.array([130,255,255])
    else:
	print('Invalid input;value must be between 1 and 3')
    image_listen()
    rospy.spin()
