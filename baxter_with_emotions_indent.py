#! /usr/bin/python
# Copyright (c) 2015, Rethink Robotics, Inc.

# Using this CvBridge Tutorial for converting
# ROS images to OpenCV2 images
# http://wiki.ros.org/cv_bridge/Tutorials/ConvertingBetweenROSImagesAndOpenCVImagesPython

# Using this OpenCV2 tutorial for saving Images:
# http://opencv-python-tutroals.readthedocs.org/en/latest/py_tutorials/py_gui/py_image_display/py_image_display.html

# rospy for the subscriber
import rospy
# ROS Image message
from sensor_msgs.msg import Image
# ROS Image message -> OpenCV2 image converter
from cv_bridge import CvBridge, CvBridgeError
# OpenCV2 for saving an image
import cv2
import numpy as np
import baxter_interface
# Imports Needed for face detection
import os
import torch
import torchvision
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision.datasets import ImageFolder
import torchvision.transforms as tt
from torchvision.utils import make_grid
import matplotlib.pyplot as plt
#%matplotlib inline
import tensorflow as tf
import pickle
import time
from PIL import Image, ImageOps, ImageEnhance

# Instantiate CvBridge
bridge = CvBridge()

global clik
global emotionDetected
global whichEmotion

def conv_block(in_channels, out_channels, pool=False):
    layers = [
        nn.Conv2d(in_channels, out_channels, kernel_size=3, padding=1),
        nn.BatchNorm2d(out_channels),
        nn.ReLU(inplace=True),
    ]
    if pool:
        layers.append(nn.MaxPool2d(2))
    return nn.Sequential(*layers)

class ResNet(nn.Module):
    def __init__(self, in_channels, num_classes):
        super().__init__()

        self.input = conv_block(in_channels, 64)

        self.conv1 = conv_block(64, 64, pool=True)
        self.res1 = nn.Sequential(conv_block(64, 32), conv_block(32, 64))
        self.drop1 = nn.Dropout(0.5)

        self.conv2 = conv_block(64, 64, pool=True)
        self.res2 = nn.Sequential(conv_block(64, 32), conv_block(32, 64))
        self.drop2 = nn.Dropout(0.5)

        self.conv3 = conv_block(64, 64, pool=True)
        self.res3 = nn.Sequential(conv_block(64, 32), conv_block(32, 64))
        self.drop3 = nn.Dropout(0.5)

        self.classifier = nn.Sequential(
            nn.MaxPool2d(6), nn.Flatten(), nn.Linear(64, num_classes)
        )

    def forward(self, xb):
        out = self.input(xb)

        out = self.conv1(out)
        out = self.res1(out) + out
        out = self.drop1(out)

        out = self.conv2(out)
        out = self.res2(out) + out
        out = self.drop2(out)

        out = self.conv3(out)
        out = self.res3(out) + out
        out = self.drop3(out)

        return self.classifier(out)

def image_callback(msg):
    global clik
    print("Received an image!")
    try:
        if clik==1:
        	cv2_img = bridge.imgmsg_to_cv2(msg, "bgr8")
		
    except CvBridgeError, e:
        print(e)
    else:
        # Save your OpenCV2 image as a jpeg 
        #cv2.imwrite('camera_image.jpeg', cv2_img)
	#img=cv.imread('colorballs.png')
	if clik==1:
		toHSV=cv2.cvtColor(cv2_img, cv2.COLOR_BGR2HSV)
		a=input('please enter a value between 1 and 3\n')
	#print(toHSV.shape)
		if whichEmotion==1:
		#YELLOW
			colorLow=np.array([20,100,100])
			colorHigh=np.array([30,255,255])
		elif a==0:
		#RED
			colorLow=np.array([0,70,50])
			colorHigh=np.array([10,255,255])
		elif a==3:
		#BLUE
		#colorLow=np.array([110,50,50])
			colorLow=np.array([50,120,150])
			colorHigh=np.array([130,255,255])
		else:
			print('Invalid input;value must be between 1 and 3')

		mask=cv2.inRange(toHSV,colorLow,colorHigh)
		#h,s,v=cv2.split(toHSV)
		#ret,thresh=cv2.threshold(mask,127,255,0)
		M=cv2.moments(mask)
	
		#print(m)
		#Calculation of centroid
		cx=int(M["m10"]/M["m00"])
		cy=int(M["m01"]/M["m00"])
		circled=cv2.circle(mask,(cx,cy),10,(0,0,255),5)
		cv2.imshow('circled',circled)
		print(cx)
		print(cy)
	
		#print(mask.shape)
		selected=cv2.bitwise_and(cv2_img,cv2_img, mask=mask)
		#print(selected.shape)
		#cv2.imshow("Original Image", cv2_img)
		#cv.imshow("th1",th1)
		#cv2.imshow('mask',mask)

		vis=np.concatenate((cv2_img,selected),axis=1)
		cv2.imwrite('OriginalandProcessed.png',vis)
		cv2.imshow('OriginalandProcessed',vis)
		
		clik=0

	cv2.waitKey(0)
	cv2.destroyAllWindows()

def emotion_callback(msg):
    global emotionDetected
    global whichEmotion

    print("Received an image!")

	#Load in necissary emotion detection files and name the class labels
    face_classifier = cv2.CascadeClassifier('/home/cvenkat/baxter_ws/Julians Code/haarcascade_frontalface_default.xml')
    model_state = torch.load('/home/cvenkat/baxter_ws/Julians Code/new_premade_emotion_detection_model_state.pth')
    class_labels = ['Neutral', 'Happy', 'Sad', 'Angry', 'surprise']
    
	# Load in the model
    model = ResNet(1, len(class_labels))
    model.load_state_dict(model_state)
    model.eval()  # Set to eval mode to change behavior of Dropout, BatchNorm


    if whichEmotion == 2:

        handcam_img = bridge.imgmsg_to_cv2(msg, "bgr8")
	gray = cv2.cvtColor(handcam_img, cv2.COLOR_BGR2GRAY)
	faces = face_classifier.detectMultiScale(gray, 1.3, 5)
		
		# if faces are detected, xywh is returned
	for (x, y, w, h) in faces:
		cv2.rectangle(handcam_img, (x, y), (x + w, y + h), (255, 0, 0), 2)
		roi_gray = gray[y : y + h, x : x + w]
		roi_gray = cv2.resize(roi_gray, (48, 48), interpolation=cv2.INTER_AREA)

		if np.sum([roi_gray]) != 0:
			roi = tt.functional.to_pil_image(roi_gray)
			roi = tt.functional.to_grayscale(roi)
			roi = tt.ToTensor()(roi).unsqueeze(0)

			# make a prediction on the ROI

			tensor = model(roi)
			pred = torch.max(tensor, dim=1)[1].tolist()
			label = class_labels[pred[0]]

			# Normalize the outputs using the Softmax function so that
			# we can interpret it as a probability distribution.
			sm = nn.Softmax(dim=1)
			sm_outputs = sm(tensor)

			# get all emotion labels
			emotions = [None]*len(tensor)
			q = 0
			for i in pred:
				emotions[q] = class_labels[i]
				q = q + 1

			print(pred)
			print(emotions)
			
			label_position = (x, y)
			cv2.putText(
				handcam_img,
				label,
				label_position,
				cv2.FONT_HERSHEY_COMPLEX,
				2,
				(0, 255, 0),
				3,
			)

			emotionDetected = 0
			whichEmotion = pred[0]
		else:
			cv2.putText(
				handcam_img,
				"No Face Found",
				(20, 60),
				cv2.FONT_HERSHEY_COMPLEX,
				2,
				(0, 255, 0),
				3,
			)

	cv2.imshow("Emotion Detector", handcam_img)

	cv2.waitKey(0)
	cv2.destroyAllWindows()

def image_listen():

    global clik
    #i=i+1
    # Define your image topic
    image_topic = "/cameras/head_camera/image"
    #image_topic = "/cameras/right_hand_camera/image"
    #image_topic = "/cameras/left_hand_camera/image"
    # Set up your subscriber and define its callback
    rospy.Subscriber(image_topic, Image, image_callback)
    # Spin until ctrl + c
    #rospy.spin()

def emotion_listen():
    global emotionDetected
    #i=i+1
    # Define your image topic
    #image_topic = "/cameras/head_camera/image"
    emotion_topic = "/cameras/right_hand_camera/image"
    #image_topic = "/cameras/left_hand_camera/image"
    # Set up your subscriber and define its callback
    rospy.Subscriber(emotion_topic, Image, emotion_callback)
    # Spin until ctrl + c
    #rospy.spin()

if __name__ == '__main__':
    global clik
    global emotionDetected
    global whichEmotion

    clik=0
    emotionDetected = 1
    whichEmotion = 0


    rospy.init_node('image_listener')
    # create instances of baxter_interface's Limb class
    limb_right = baxter_interface.Limb('right')
    limb_left = baxter_interface.Limb('left')
    home_right = {'right_s0': 0.70, 'right_s1': 0.00, 'right_w0': -
	0.0, 'right_w1': 0.0, 'right_w2': 0.0, 'right_e0': 1.57,
	'right_e1': 0.0}
    home_left = {'left_s0': -0.08, 'left_s1': -1.00, 'left_w0': 0.67,
       'left_w1': 1.03, 'left_w2': -0.50, 'left_e0': -1.18, 'left_e1':
        1.94}
    # move arms to position
    limb_right.move_to_joint_positions(home_right)
    limb_left.move_to_joint_positions(home_left)

    while whichEmotion == 2:
	emotion_listen()
	
    a = np.array([-0.08, 0.150, 0.3])
    b = np.array([-0.8, -1, -1.2])
    #r = rospy.Rate(100)
    for x in a:
        for y in b:
	    home_left = {'left_s0': x, 'left_s1': y, 'left_w0': 0.67,
	   'left_w1': 1.03, 'left_w2': -0.50, 'left_e0': -1.18, 'left_e1':
	    1.94}
	    # move left arm to home position
	    limb_left.move_to_joint_positions(home_left)
	    clik =1
            image_listen()
	    #r.sleep()

    #image_listen()
    quit()
